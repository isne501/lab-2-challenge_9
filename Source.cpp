#include <iostream>
using namespace std;

char Table(int i);
void ShowTable(int arr[9]);
void playerMove(int arr[9]);
int checkwin(int arr[9]);
int minimax(int arr[9], int player);
void aiMove(int arr[9]);

void main()
{
		int arr[9] = {0}; //initial table
		int player;

		cout << "Player first [1] , AI first [2] : "; //player select turn
		cin >> player;

		while(player >2 || player <1 )
		{
				cout << "Incorrect input, Try again! : ";
				cin >> player;
		}
	
		int count = 0;
		while (true) //game loop
		{
			cout << "[  Turn " << count+1 << "  ] " << endl; //show turn
			if (player == 1) //if player first
			{
				playerMove(arr);
				cout << "Player Turn" << endl;
				ShowTable(arr);
				player = 2;
				count++; //cound round
			}
			else if (player == 2) //if plater second
			{
				aiMove(arr);
				cout << "AI Turn" << endl;
				ShowTable(arr);
				player = 1;
				count++; //count round 
			}

			if (checkwin(arr) == 1) //If checkwin(arr) return 1 that mean player lose because -1 is asign to AI
			{
				cout << "\nBot Win!! --> AI said 'You are the weekness that I have ever seen!" << endl;
				break;
			}
			else if (checkwin(arr) == -1) //If checkwin(arr) return -1 that mean player win because -1 is asign to player
			{
				cout << "\nYou win...! --> AI said 'Maybe I don't concentrate the game'" << endl;
				break;
			}
			else if (count == 9) //if full table means Draw
			{
				cout << "\nDraw...!" << endl;
				break;
			}
		}
	system("PAUSE");
}

char Table(int i) //assign X,O, blank to table
{
	switch (i)
	{
	case -1:
		return 'X';
	case 0:
		return ' ';
	case 1:
		return 'O';
	}
}

void ShowTable(int arr[9]) //Output the table
{
	cout << "\t\t\t " << Table(arr[0]) << " | " << Table(arr[1]) << " | " << Table(arr[2]) << endl;
	cout << "\t\t\t-----------" << endl;
	cout << "\t\t\t " << Table(arr[3]) << " | " << Table(arr[4]) << " | " << Table(arr[5]) << endl;
	cout << "\t\t\t-----------" << endl;
	cout << "\t\t\t " << Table(arr[6]) << " | " << Table(arr[7]) << " | " << Table(arr[8]) << endl;
}

void playerMove(int arr[9]) //Player turn
{
	int pMove;
	cout << "Player select move [1-9]: "; //Select place
	cin >> pMove;
	while (pMove < 1 || pMove>9) //if wrong input 
	{
		cout << "Incorrect input ,Try again : ";
		cin >> pMove;
	}
	
	while (arr[pMove-1] != 0 ) //if place is not blank and player select
	{
			cout << "That place is selected,Try again : ";
			cin >> pMove;
	}	
	if (arr[pMove - 1] == 0) //if place is blank
	{
		arr[pMove - 1] = -1; //assign this place with X
	}
	
}

int checkwin(int arr[9]) //check win
{
	int win[8][3] = { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 }, { 0, 4, 8 }, { 2, 4, 6 } }; //condition to win
	for (int i = 0; i < 8; ++i)
	{
		if (arr[win[i][0]] == arr[win[i][2]] && arr[win[i][0]] != 0 && arr[win[i][0]] == arr[win[i][1]])
		{
			return arr[win[i][2]]; //if win
		}
	}
	return 0; //continue game 
}

int minimax(int arr[9], int player) //find the place for AI
{
	int move = -1;
	int score = -2;
	int win = checkwin(arr);
	if (win != 0)
	{
		return win * player;
	}
	for (int i = 0; i < 9; ++i)
	{
		if (arr[i] == 0)
		{
			arr[i] = player;
			int tmp = -minimax(arr, player*-1);
			if (tmp > score)
			{
				score = tmp;
				move = i;
			}
			arr[i] = 0;
		}
	}
	if (move == -1)
	{
		return 0;
	}
	return score;
}

void aiMove(int arr[9]) //AI turn 
{
	int move = -1;
	int score = -2;

	for (int i = 0; i < 9; ++i)
	{
		if (arr[i] == 0)
		{
			arr[i] = 1;
			int tmpScore = -minimax(arr, -1);
			arr[i] = 0;
			if (tmpScore > score)
			{
				score = tmpScore;
				move = i;
			}
			if (arr[4] == 0)
			{
				move = 4;
			}
		}
	}
	arr[move] = 1;
}
